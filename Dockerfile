FROM ejectedspace/stack-musl:latest as builder

ADD . /work
WORKDIR /work

RUN apk update
RUN apk add file-dev postgresql-dev
RUN hpack
RUN cabal update
RUN cabal install

FROM alpine:latest

COPY --from=builder /root/.cabal/bin/pastor /bin/pastor
RUN apk add libmagic libpq gmp libffi

ENV PASTOR_PG_HOST postgresql
ENV PASTOR_PG_PORT 5432
ENV PASTOR_PG_USER pastor
ENV PASTOR_PG_PASS pastor
ENV PASTOR_PG_DB pastor
ENV PASTOR_PORT 3000
ENV PASTOR_LOG INFO
ENV PASTOR_DATA_DIR /pastor

RUN mkdir -p /pastor
ADD index.txt /usr/local/share/pastor/index.txt
VOLUME /pastor

CMD ["/bin/pastor"]
