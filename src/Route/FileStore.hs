{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}

module Route.FileStore
  ( fileStoreGet
  , fileStorePost
  , fileStoreDelete
  , fileStorePut
  ) where

import qualified Data.Text.Lazy as TL

import Web.Scotty

import Hasql.Session (Session)
import Hasql.Statement (Statement(..))
import qualified Hasql.Connection as Connection
import qualified Hasql.Session as Session

import Sessions
import Errors
import Config

import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Data.Maybe
import qualified Data.List.Split as S
import Data.String.Utils

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Lazy.UTF8 as BL8

import Control.Monad.IO.Class

import Magic

import Network.HTTP.Types
import Network.Wai.Parse

import System.Log.Logger
import System.IO.Unsafe

data FileBundle = FileBundle
  { bundleFile :: BL.ByteString
  , bundleExt :: String
  , bundleMime :: String
  , bundleCharset :: String
  } deriving Show

getFileMime fi magic = unsafePerformIO $ magicString magic (BL8.toString $ BL.take 32 $ fileContent fi)

fileStorePost conn = do
    token <- liftIO shortyGen
    adminKey <- liftIO shortyGen
    fs <- files
    magic <- liftIO $ magicOpen [MagicMime]
    liftIO $ magicLoadDefault magic
    let fs' = [ FileBundle
          (fileContent fi)
          (Prelude.last $ S.splitOn "." $ T.unpack $ TE.decodeUtf8 $ fileName fi)
          (Prelude.head $ S.splitOn "; " $ getFileMime fi magic)
          (replace "charset=" "" $ Prelude.last $ S.splitOn "; " $ getFileMime fi magic) | (fieldName,fi) <- fs ]
    host <- header "Host"
    text $ mconcat [unsafePerformIO $ evaluateStuff files conn host | files <- fs' ]

evaluateStuff :: FileBundle -> Connection.Connection -> Maybe TL.Text -> IO TL.Text
evaluateStuff files conn host =
  fileStorePost' files conn >>= \case
      Left (name, Session.QueryError qsql qres qerr) ->
        return $ "failed to upload " `TL.append` TL.pack name `TL.append` ": " `TL.append` TL.pack (show qerr) `TL.append` "\n"
      Right (name, restoken, reskey) ->
        return $ TL.concat ["Token: ",TL.pack reskey, "\n", "URL: https://", fromMaybe "couldn't find host" host, "/", TL.pack restoken, "\n"]

fileStorePost' :: FileBundle -> Connection.Connection -> IO (Either (String, Session.QueryError) (String, String, String))
fileStorePost' file conn =
  if bundleCharset file == "binary"
    then do
      liftIO $ debugM "pastor.web" $ bundleCharset file
      token <- shortyGen
      adminKey <- shortyGen
      BL.writeFile (pastorDataDir getConfig ++ token) (bundleFile file)
      magic <- liftIO $ magicOpen [MagicMime]
      liftIO $ magicLoadDefault magic
      mimetype <- magicFile magic (pastorDataDir getConfig ++ token)
      res <- Session.run (postFile
                          token
                          adminKey
                          True
                          Nothing
                          (T.pack $ bundleExt file)
                          (T.pack $ Prelude.head $ S.splitOn "; " mimetype)
                         ) conn
      case res of
        Left err -> return $ Left (bundleExt file, err)
        Right res -> return $ Right (bundleExt file, token, adminKey)
    else do
      liftIO $ debugM "pastor.web" $ bundleCharset file
      token <- shortyGen
      adminKey <- shortyGen
      res <- Session.run (postFile
                          token
                          adminKey
                          False
                          (Just (TE.decodeUtf8 $ BL.toStrict $ bundleFile file))
                          (T.pack $ bundleExt file)
                          (T.pack $ bundleMime file)
                         ) conn
      case res of
        Left err -> return $ Left (bundleExt file, err)
        Right res -> return $ Right (bundleExt file, token, adminKey)

fileStoreGet conn = do
    token' <- param "env"
    let token = Prelude.head $ S.splitOn "." token'
    res <- liftIO $ Session.run (getFile token) conn
    case res of
      Left (Session.QueryError qsql qres qerr) -> do
        liftIO $ debugM "pastor.web" $ show qsql ++ show qres ++ show qerr
        handleDBError qerr
      Right (short, isBinary, content, filePath, fileMime) ->
        if isBinary
          then do
            setHeader "Content-Type" $ TL.fromStrict fileMime
            file $ T.unpack $ T.pack (pastorDataDir getConfig) `T.append` short
          else do
            liftIO $ debugM "pastor.web" $ show $ fromJust content
            text $ TL.fromStrict $ fromJust content

fileStoreDelete conn = do
    token' <- param "env"
    adminKey <- param "token"
    let token = Prelude.head $ S.splitOn "." token'
    res <- liftIO $ Session.run (deleteFile token adminKey) conn
    case res of
      Left (Session.QueryError qsql qres qerr) -> handleDBError qerr
      Right _ -> do
        liftIO $ debugM "pastor.web" "url deleted"
        status status200

fileStorePut conn = do
    token' <- param "env"
    let token = Prelude.head $ S.splitOn "." token'
    adminKey <- param "token"

    fs <- files

    magic <- liftIO $ magicOpen [MagicMime]
    liftIO $ magicLoadDefault magic
    let fs' = [ FileBundle
          (fileContent fi)
          (Prelude.last $ S.splitOn "." $ T.unpack $ TE.decodeUtf8 $ fileName fi)
          (Prelude.head $ S.splitOn "; " $ getFileMime fi magic)
          (replace "charset=" "" $ Prelude.last $ S.splitOn "; " $ getFileMime fi magic) | (fieldName,fi) <- fs ]
    host <- header "Host"
    text $ unsafePerformIO $ evaluateStuffPut (head fs') conn token adminKey host

evaluateStuffPut :: FileBundle -> Connection.Connection -> String -> String -> Maybe TL.Text -> IO TL.Text
evaluateStuffPut files conn token adminKey host =
  fileStorePostPut files conn token adminKey >>= \case
      Left (name, Session.QueryError qsql qres qerr) ->
        return $ "failed to upload " `TL.append` TL.pack name `TL.append` ": " `TL.append` TL.pack (show qerr) `TL.append` "\n"
      Right (name, restoken) ->
        return $ "https://" `TL.append` fromMaybe "couldn't find host" host `TL.append` "/" `TL.append` TL.pack restoken `TL.append` "\n"

fileStorePostPut :: FileBundle -> Connection.Connection -> String -> String -> IO (Either (String, Session.QueryError) (String, String))
fileStorePostPut file conn token adminKey =
  if bundleCharset file == "binary"
    then do
      liftIO $ debugM "pastor.web" $ bundleCharset file
      BL.writeFile (pastorDataDir getConfig ++ token ++ "." ++ bundleExt file) (bundleFile file)
      magic <- liftIO $ magicOpen [MagicMime]
      liftIO $ magicLoadDefault magic
      mimetype <- magicFile magic (pastorDataDir getConfig ++ token ++ "." ++ bundleExt file)
      res <- Session.run (putFile
                          token
                          adminKey
                          True
                          Nothing
                          (T.pack $ bundleExt file)
                          (T.pack $ Prelude.head $ S.splitOn "; " mimetype)
                         ) conn
      case res of
        Left err -> return $ Left (bundleExt file, err)
        Right res -> return $ Right (bundleExt file, token)
    else do
      liftIO $ debugM "pastor.web" $ bundleCharset file
      res <- Session.run (putFile
                          token
                          adminKey
                          False
                          (Just (TE.decodeUtf8 $ BL.toStrict $ bundleFile file))
                          (T.pack $ bundleExt file)
                          (T.pack $ bundleMime file)
                         ) conn
      case res of
        Left err -> return $ Left (bundleExt file, err)
        Right res -> return $ Right (bundleExt file, token)
