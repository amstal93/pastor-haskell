{-# LANGUAGE OverloadedStrings #-}

module Routes (routes) where

import System.Directory

import Web.Scotty

import System.IO.Unsafe

import qualified Hasql.Connection as Connection

import Route.ShortLinks
import Route.FileStore

import Config

routes :: Connection.Connection -> ScottyM ()
routes conn = do
  if unsafePerformIO $ doesFileExist $ pastorDataDir getConfig ++ "/index.txt"
    then get "/" $ file $ pastorDataDir getConfig ++ "/index.txt"
    else get "/" $ file $ "/usr/local/share/pastor/index.txt"
  
  get "/u/:env" $ shortLinkGet conn
  post "/u" $ shortLinkPost conn
  delete "/u/:env" $ shortLinkDelete conn
  put "/u/:env" $ shortLinkPut conn


  get "/:env" $ fileStoreGet conn
  post "/" $ fileStorePost conn
  put "/:env" $ fileStorePut conn
  delete "/:env" $ fileStoreDelete conn


